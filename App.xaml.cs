﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;

namespace ListMethods
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public class ClassComparer : Comparer<Type>
        {
            public override int Compare(Type t1, Type t2)
            {
                return (new Comparer(new System.Globalization.CultureInfo("en-US"))).Compare(t1.Name, t2.Name);
            }
        }
        public static string ShowNames(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                try
                {
                    Directory.SetCurrentDirectory(path);
                }
                catch (DirectoryNotFoundException _)
                { return "Directory not found! Please provide a correct path to directory!"; }

                string[] files = Directory.GetFiles(path, "*.dll");
                ArrayList classes = new ArrayList();
                Assembly SampleAssembly;
                foreach (string f in files)
                {
                    SampleAssembly = Assembly.LoadFrom(f);
                    classes.AddRange(SampleAssembly.GetTypes());
                }
                classes.Sort(new ClassComparer());

                ArrayList strings = new ArrayList();
                foreach (Type t in classes)
                {
                    strings.Add(t.Name);
                    foreach (MethodInfo f in t.GetMethods(BindingFlags.Static
                    | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic))
                        if (!f.IsPrivate)
                            strings.Add("- " + f.Name);
                }
                return String.Join('\n', strings.ToArray());
            }
            else
                return "No path, please provide a path to directory!";
        }
    }
}
